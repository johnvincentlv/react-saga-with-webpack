import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import { Provider } from 'react-redux';
import _ from 'lodash'
import moment from 'moment';

import Root from './views/genricComponents/Root';

const App = () => {
    return (
        <Router >
            <Switch>
                <Route exact path="/" component={Root} />
            </Switch>
        </Router>
    )
}

export default App;