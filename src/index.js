import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from './app.jsx';
import configureStore from './store/index';
import 'styles/main';

const store = configureStore();
ReactDOM.render( 
<Provider store = {store} >
    <React.Fragment >
    <App/>
    </React.Fragment>
 </Provider > , document.getElementById('app'));