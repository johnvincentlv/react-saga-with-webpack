
const apiPaths = {
    widgets:'/api/cancellation/widgets', 
    mic:'/api/cancellation/MyntraInitiatedCancellation', 
    cancellationsubcategory:'/api/cancellation/cancellationsubcategory',
    trendofsubcategory:'/api/cancellation/trendofsubcategory',
    micancellationbucketlevel:'/api/cancellation/micancellationbucketlevel',
    trendMICancellationBucketlevel:'/api/cancellation/trendMICancellationBucketlevel',
    cancellationdeepdive:'/api/cancellation/cancellationdeepdive',
}


export default apiPaths;