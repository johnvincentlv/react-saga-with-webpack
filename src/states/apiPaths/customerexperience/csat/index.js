
const apiPaths = {
    widget:'/api/dashboard/customerexperience/csat/widgets', 
    csatOverall:'/api/dashboard/customerexperience/csat/csatOverall', 
    TrendcsatLogistic:'/api/dashboard/customerexperience/csat/TrendcsatLogistic',
    TrendcsatRegional:'/api/dashboard/customerexperience/csat/TrendcsatRegional',
    csatRegionalvsML:'/api/dashboard/customerexperience/csat/csatRegionalvsML',
    C2DvsCsat:'/api/dashboard/customerexperience/csat/C2DvsCsat',
    csatDeliverystuff:'/api/dashboard/customerexperience/csat/csatDeliverystuff',
    csatDeliverystuff1:'/api/dashboard/customerexperience/csat/csatDeliverystuff1',
    csatcourierwise:'/api/dashboard/customerexperience/csat/csatcourierwise',
    csatregion:'/api/dashboard/customerexperience/csat/csatregion',
    csatregionaldeepdive:'/api/dashboard/customerexperience/csat/csatregionaldeepdive',
    csatassociationanalysis:'/api/dashboard/customerexperience/csat/csatassociationanalysis',
    csat_3p_partners:'/api/dashboard/customerexperience/csat/csat_3p_partners',
}

export default apiPaths;