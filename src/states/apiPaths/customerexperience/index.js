import nps from './nps';
import csat from './csat';

const apiPaths = {
    nps:nps,
    csat:csat,
}

export default apiPaths;