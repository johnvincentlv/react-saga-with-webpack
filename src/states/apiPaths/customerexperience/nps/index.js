
const apiPaths = {
    widgets:'/api/dashboard/customerexperience/nps/widgets', 
    overall:'/api/dashboard/customerexperience/nps/overall', 
    trend:'/api/dashboard/customerexperience/nps/trend',
    region:'/api/dashboard/customerexperience/nps/region',
    speed:'/api/dashboard/customerexperience/nps/speed',
    breach:'/api/dashboard/customerexperience/nps/breach', 
    csat:'/api/dashboard/customerexperience/nps/csat',
}


export default apiPaths;