
const apiPaths = {
    widgets:'/api/dashboard/widgets',
    pie:'/api/dashboard/pie',
    areachart:'/api/dashboard/AreaChart',
}


export default apiPaths;