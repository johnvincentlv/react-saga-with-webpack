import Home from './home';
import Customerexperience from './customerexperience';
import Performanceforward from './performanceforward';
import cancellation from './cancellation';
import Returns from './returns';
import Rnps from './rnps';
const apiPaths = {
    login:'/api/login',
    logout:'/api/sso/logout',
    getUser:'/api/user',
    home:Home,
    customerexperience:Customerexperience,
    performanceforward:Performanceforward,
    returns:Returns,
    rnps: Rnps,
    cancellation:cancellation,
}

export default apiPaths;