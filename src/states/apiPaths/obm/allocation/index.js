
const apiPaths = {
    widgets:'/api/dashboard/obm/allocation/widgets',
    allocationbywh:'/api/dashboard/obm/allocation/allocationbywh',
    allocationlogistics:'/api/dashboard/obm/allocation/allocationlogistics',
    trendlogistics:'/api/dashboard/obm/allocation/trendlogistics',
    trendallocation:'/api/dashboard/obm/allocation/trendallocation'
}


export default apiPaths;