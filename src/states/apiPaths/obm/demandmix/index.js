
const apiPaths = {
  
    demandmix:'/api/dashboard/obm/demandmix/demanmixmap',
    overview:'/api/dashboard/obm/demandmix/overview', 
    tierwise:'/api/dashboard/obm/demandmix/tierwise'
}


export default apiPaths;