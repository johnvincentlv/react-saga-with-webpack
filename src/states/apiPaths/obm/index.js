import overview from './overview'
import deepdive from './deepdive'


const apiPaths = {
    overview:overview,
    deepdive:deepdive
  }


export default apiPaths;

