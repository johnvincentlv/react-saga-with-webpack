
const apiPaths = {
    revenue:'/api/dashboard/obm/revenue/revenue', 
    revenuebybrands:'/api/dashboard/obm/revenue/revenuebybrands',
    revenuebycommercial:'/api/dashboard/obm/revenue/revenuebycomnercial', 
    revenuebyproduct:'/api/dashboard/obm/revenue/revenuebyproduct',
    topbrands:'/api/dashboard/obm/revenue/topbrands'
}


export default apiPaths;