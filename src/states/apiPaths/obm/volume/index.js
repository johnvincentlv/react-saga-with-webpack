
const apiPaths = {
    volume:'/api/dashboard/obm/volume/volume', 
    volumebybrands:'/api/dashboard/obm/volume/volumeB', 
    volumebycommercial:'/api/dashboard/obm/volume/tierwise', 
    volumebyproduct:'/api/dashboard/obm/volume/demanmixmap', 
    topbrands:'/api/dashboard/obm/volume/overview', 
}


export default apiPaths;