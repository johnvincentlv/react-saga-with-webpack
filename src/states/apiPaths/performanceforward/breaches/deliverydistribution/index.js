
const apiPaths = {
    widgets:"/api/dashboard/performanceforward/breaches/deliverydistribution/Widgets",
    deliverymiss:"/api/dashboard/performanceforward/breaches/deliverydistribution/deliverymissanalysis",
    trenddelivery:"/api/dashboard/performanceforward/breaches/deliverydistribution/trendofdeliverymiss",
    breachhistogram:"/api/dashboard/performanceforward/breaches/deliverydistribution/branchhistogram",
    breachdeepdive:"/api/dashboard/performanceforward/breaches/deliverydistribution/breacheepdive"
}


export default apiPaths;