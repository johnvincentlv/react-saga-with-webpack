
const apiPaths = {
    widgets:"/api/dashboard/performanceforward/breaches/widgets",
    cpdbreaches:"/api/dashboard/performanceforward/breaches/cpdbreaches",
    apdbreach:"/api/dashboard/performanceforward/breaches/apdbreach",
    getcpdinvimp:"/api/dashboard/performanceforward/breaches/getcpdinvimp",
    apdml3p:"/api/dashboard/performanceforward/breaches/apdml3p", 
    cpdageingview:"/api/dashboard/performanceforward/breaches/cpdageingview",
    apdageingview:"/api/dashboard/performanceforward/breaches/apdageingview",

    lanewiseanalysis:"/api/dashboard/performanceforward/breaches/lanewiseanalysis",
    whlevelanalysis:"/api/dashboard/performanceforward/breaches/whlevelanalysis",
    cpdstage:"/api/dashboard/performanceforward/breaches/cpdstage",
    cpdregional:"/api/dashboard/performanceforward/breaches/cpdregional", 
    apdstagewise:"/api/dashboard/performanceforward/breaches/apdstagewise",
    getapdregional:"/api/dashboard/performanceforward/breaches/getapdregional",
    apdbreachanlysis:"/api/dashboard/performanceforward/breaches/apdbreachanlysis",

    cpdlanewiseanalysis:"/api/dashboard/performanceforward/breaches/cpdlanewiseanalysis"
}

export default apiPaths;