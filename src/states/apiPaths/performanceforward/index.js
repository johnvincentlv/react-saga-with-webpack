import speed from './speed';
import breaches from './breaches';

const apiPaths = {
    speed:speed,
    breaches:breaches,
}

export default apiPaths;