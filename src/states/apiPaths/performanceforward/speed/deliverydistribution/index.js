
const apiPaths = {
    widgets:'/api/dashboard/performanceforward/speed/deliverydistribution/widgets',
    forwardspeeddistribution:'/api/dashboard/performanceforward/speed/deliverydistribution/forwardspeeddistribution',
    trendforward:'/api/dashboard/performanceforward/speed/deliverydistribution/trendforward',
    warehouse:'/api/dashboard/performanceforward/speed/deliverydistribution/Warehouse',
    delayspreadanalysis:'/api/dashboard/performanceforward/speed/deliverydistribution/Delayspreadanalysis',
    forwarddistributiontable:'/api/dashboard/performanceforward/speed/deliverydistribution/forwarddistributiontable',
}


export default apiPaths;