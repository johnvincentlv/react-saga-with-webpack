
const apiPaths = {
    c2d:"/api/dashboard/performanceforward/speed/c2dtimeline",
    c2dbreakup:'/api/dashboard/performanceforward/speed/c2dbreakup',
    widgets:"/api/dashboard/performanceforward/speed/widgets",
    c2dBreakup1:"/api/dashboard/performanceforward/speed/c2dbreakup1",
    overallc2s:"/api/dashboard/performanceforward/speed/overallc2s",
    c2dBreakup2:'/api/dashboard/performanceforward/speed/c2dbreakup2',
    c2swhwise:'/api/dashboard/performanceforward/speed/c2swhwise',
    overallanalysis:'/api/dashboard/performanceforward/speed/overallanalysis',
    overalls2d:'/api/dashboard/performanceforward/speed/overalls2d',
    s2dwhwise:'/api/dashboard/performanceforward/speed/s2dwhwise',
    s2dsnapshot:'/api/dashboard/performanceforward/speed/s2dsnapshot',
    s2d3ppartners:'/api/dashboard/performanceforward/speed/s2d3ppartners',
    s2dsnapshots2i_i2d:'/api/dashboard/performanceforward/speed/s2dsnapshots2i_i2d',
    s2dloaddistribution:'/api/dashboard/performanceforward/speed/s2dloaddistribution',
    s2dlanewise:'/api/dashboard/performanceforward/speed/s2dlanewise',

    

    speeddeepdiveS2I:'/api/dashboard/performanceforward/speed/speeddeepdiveS2I',
    speeddeepdiveS2D:'/api/dashboard/performanceforward/speed/speeddeepdiveS2D',
    speeddeepdiveI2D:'/api/dashboard/performanceforward/speed/speeddeepdiveI2D',
}


export default apiPaths;