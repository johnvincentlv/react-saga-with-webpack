
import home from "./home"
import deliverydistribution from "./deliverydistribution"

const apiPaths = {
    home:home,
    deliverydistribution:deliverydistribution,
}


export default apiPaths;