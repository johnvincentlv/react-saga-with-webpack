
const apiPaths = {
    widgets:'/api/dashboard/rnps/deepdive/widgets',
    cxpectrum:'/api/dashboard/rnps/deepdive/cxpectrum',
    level1drilldown:'/api/dashboard/rnps/deepdive/level1drilldown',
    topparameters:'/api/dashboard/rnps/deepdive/topparameters',
    socialmedia:'/api/dashboard/rnps/deepdive/socialmedia',
    brandparameters:'/api/dashboard/rnps/deepdive/brandparameters',
    brandattribute:'/api/dashboard/rnps/deepdive/brandattribute',
    primarybrandl1:'/api/dashboard/rnps/deepdive/primarybrandl1',
    brandl1audience:'/api/dashboard/rnps/deepdive/brandl1audience',
    brandl1overall:'/api/dashboard/rnps/deepdive/brandl1overall',
    brandl1zonewise:'/api/dashboard/rnps/deepdive/brandl1zonewise',
    trendofrnps:'/api/dashboard/rnps/deepdive/trendofrnps',
    brands:'/api/dashboard/rnps/deepdive/brands',

    brandparameterattribution:'/api/dashboard/rnps/deepdive/brandparameterattribution',


    mastercategory_myntra:'/api/dashboard/rnps/deepdive/mastercategorymyntra',
    trendofmaster:'/api/dashboard/rnps/deepdive/trendofmaster',
    materOverall:'/api/dashboard/rnps/deepdive/MaterOverall',
    masterzonewise:'/api/dashboard/rnps/deepdive/masterzonewise',
    masteraudience:'/api/dashboard/rnps/deepdive/masteraudience',
}


export default apiPaths;