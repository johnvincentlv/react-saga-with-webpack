
const apiPaths = {
    widgets:'/api/dashboard/rnps/overview/widgets',
    overview:'/api/dashboard/rnps/overview/overview',
    rnpsoverallsummary:'/api/dashboard/rnps/overview/rnpsoverallsummary',
    rnpsregionaltrend:'/api/dashboard/rnps/overview/rnpsregionaltrend',
    rnpsgender:'/api/dashboard/rnps/overview/rnpsgender',
    rnpsage:'/api/dashboard/rnps/overview/rnpsage',
    rnpsoverall:'/api/dashboard/rnps/overview/rnpsoverall',
    rnpsregional:'/api/dashboard/rnps/overview/rnpsregional',
    rnpsnorthzone:'/api/dashboard/rnps/overview/rnpsnorthzone', 
    rnpssouthzone:'/api/dashboard/rnps/overview/rnpssouthzone', 
    rnpseastzone:'/api/dashboard/rnps/overview/rnpseastzone', 
    rnpswestzone:'/api/dashboard/rnps/overview/rnpswestzone',
    rnpsmale:'/api/dashboard/rnps/overview/rnpsmale',
    rnpsfemale:'/api/dashboard/rnps/overview/rnpsfemale',
}


export default apiPaths;