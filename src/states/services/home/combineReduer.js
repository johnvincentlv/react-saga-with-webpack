
import { combineReducers } from 'redux';

import component1 from './component1/reducer';
import component2 from './component2/reducer';

const Customerexperience = combineReducers({
    component1,
    component2
});

export default Customerexperience;