import { put, takeEvery, all } from 'redux-saga/effects';
import sagacomponent1 from './component1/saga'
import sagacomponent2 from './component1/saga'
import actionType from './actionTypes'

export function* HomeSaga() {
    yield takeEvery("SAGA_CALL_COMPONENT_1", sagacomponent1) 
    yield takeEvery("SAGA_CALL_COMPONENT_2", sagacomponent2)
}