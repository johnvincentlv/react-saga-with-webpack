import actionType from '../actionTypes'

const actionComponent1 = () => {
    return{
        type: "SAGA_CALL_COMPONENT_1",
    }
}

export default actionComponent1;