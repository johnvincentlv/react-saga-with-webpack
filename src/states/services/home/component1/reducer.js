import * as types from '../actionTypes';
import initialState from './initialState';

const HomeReducer = (state = initialState, action) => {
    switch (action.type) {
        case "GET_COMPONENT_1":
            return {
                ...state,
                isLoaded:true,
                data: action.payload
            };
        case "GET_COMPONENT_1_ERROR":
            return {
                ...state,
                isLoaded:false,
                errormessage: action.payload.data.error.message
            };
        case "GET_COMPONENT_1_REQUEST":
            return {
                ...state,
                isLoaded:false
            };
        default:
            return state;
    }

};


export default HomeReducer;