import actionType from '../actionTypes'

const actionSaga = (Param) => ({
    type: actionType.SAGA_CALL_COMPONENT_2,
    payload: Param
})

export default actionSaga;