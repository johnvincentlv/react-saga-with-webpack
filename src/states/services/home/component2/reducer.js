import * as types from '../actionTypes';
import initialState from './initialState';

const Component1Reducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_COMPONENT_2:
            return {
                ...state,
                isLoaded:true,
                component2data: action.payload.data
            };
        case types.GET_COMPONENT_2_ERROR:
            return {
                ...state,
                isLoaded:false,
                errormessage: action.payload.data.error.message
            };
        case types.GET_COMPONENT_2_REQUEST:
            return {
                ...state,
                isLoaded:false
            };
        default:
            return state;
    }

};


export default Component1Reducer;