import {
    put,all
  } from 'redux-saga/effects';
  import {HomeSaga} from './home/combineSaga';

export default function* rootSaga() {
    yield all([
        HomeSaga(),
    ])
  }