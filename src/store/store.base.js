import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import rootReduer from '../states/services/rootReduer';

const appReducer = combineReducers({
    rootReduer,
    routing: routerReducer
});

const rootReducer = (state, action) => {
    return appReducer(state, action);
};

export default rootReducer;