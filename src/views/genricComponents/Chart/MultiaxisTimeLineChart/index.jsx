import React, { Component } from 'react';
import Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting'
import ExportingData from 'highcharts/modules/export-data'
import HichartsOfflineExporting from 'highcharts/modules/offline-exporting';
Exporting(Highcharts)
ExportingData(Highcharts)
HichartsOfflineExporting(Highcharts)

export default class Area extends Component {

    static defaultProps = {
        data: {
            id: "Line",
            title: "Demand Mix",
            tooltip: '{series.name}: <b>{point.y}%</b>',
            ytitle: "",
            height: 270,
            legend: {
                symbolWidth: 20,
                enabled: true
            },
            splittooltip: false,
            turboThreshold: 5000,
            plotOptions: {
                series: {
                    label: {
                        connectorAllowed: false
                    },
                    marker: {
                        enabled: true,
                        symbol: 'circle',
                        radius: 2
                    }
                }
            },
            chartType: "spline",
            color: '#12b0f2',
            enableSelection: true,
            xAxis: {
                type: 'datetime'
            },
            yAxis:[{
                title:{
                    text:"value"
                }
            }],
            data:[  
                {
                    name: 'Rainfall',
                    type: 'spline',
                    yAxis: 1,
                    data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4],
                    tooltip: {
                        valueSuffix: ' mm'
                    }
            
                },{
                    name: 'Temperature',
                    type: 'spline',
                    data: [7.0, 6.9, 9.5, 14.5, 18.2, 21.5, 25.2, 26.5, 23.3, 18.3, 13.9, 9.6],
                    tooltip: {
                        valueSuffix: ' °C'
                    }
                }]
        }
    };

    buildChart = {}

    componentDidMount() {
        if (this.AreaSVG !== undefined) {
            this.buildGraph(this.props.data);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.AreaSVG !== undefined) {
            this.buildGraph(nextProps.data);
        }
    }

    buildGraph(data) {

        this.buildChart = Highcharts.chart(this.AreaSVG.id, {
            credits: {
                enabled: false
            },
            chart: { width: window.size, height: data.height, zoomType: 'x', type: data.chartType },
            title: {
                text: data.title,
                style: {
                    font: "normal 14px 'Open Sans', sans-serif"
                }
            },
            xAxis: data.xAxis,
            tooltip: data.tooltip,
            turboThreshold: 10000,
            legend: data.legend,
            yAxis:data.yAxis,
            plotOptions: data.plotOptions,
            title: {
                text: ''
            },
            exporting: false,
            series: data.data,
        });
    }

    exportData(filename) {
        this.buildChart.exportChartLocal({
            filename: filename,
            type: 'image/png'
        })
    }

    render() {
        const { width, height, id } = this.props.data;
        return <div id={id} className="area-chart-v" ref={el => { this.AreaSVG = el; }} />
    }

}
