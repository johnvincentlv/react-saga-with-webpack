import React, { Component } from 'react';
import Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting'
// import ExportingData from 'highcharts/modules/export-data'
import HichartsOfflineExporting from 'highcharts/modules/offline-exporting';
Exporting(Highcharts)
// ExportingData(Highcharts)
HichartsOfflineExporting(Highcharts)

export default class StackedArea extends Component {
    enableSelection = false
    static defaultProps = {
        data: {
            id: "stacked-area",
            title: "C2D Breakup",
            tooltip: '{series.name}: <b>{point.percentage:.1f}%</b>',
            ytitle: "Conversations",
            margin: { top: 40, left: 80, bottom: 30, right: 40 },
            height: 225,
            color: '#12b0f2',
            innerSize: '0%',
            enableSelection: false,
            legend:true,
            marker:true,
            xAxis: {
                type: 'datetime'
            },
            data: [{
                name: 'Planned',
                color: 'purple',
                data: [{
                    x: 1533081600000,
                    y: 1161
                },
                {
                    x: 1535760000000,
                    y: 1245
                }, {
                    x: 1537760000000,
                    y: 725
                }, {
                    x: 1539760000000,
                    y: 1145
                }, {
                    x: 1542760000000,
                    y: 945
                }, {
                    x: 1545760000000,
                    y: 1245
                }],
            }, {
                name: 'Actual',
                color: '#fb00a6',
                data: [{
                    x: 1533081600000,
                    y: 116
                },
                {
                    x: 1535760000000,
                    y: 124
                }, {
                    x: 1537760000000,
                    y: 735
                }, {
                    x: 1539760000000,
                    y: 145
                }, {
                    x: 1542760000000,
                    y: 9415
                }, {
                    x: 1545760000000,
                    y: 125
                }],
            },]
        }
    };

    buildChart = {}

    componentDidMount() {
        if (this.StackedAreaSVG !== undefined) {
            this.buildGraph(this.props.data);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.StackedAreaSVG !== undefined) {
            this.buildGraph(nextProps.data);
        }
    }

    buildGraph(data) {

        this.buildChart = Highcharts.chart(this.StackedAreaSVG.id, {
            chart: {
                type: 'area',
                height: data.height,
                zoomType: 'x'
            },
            title: {
                text: ""
            },
            credits: false,
            exporting: false,
            xAxis: data.xAxis,
            yAxis: {
                title: {
                    text: ''
                },
                labels: {
                    formatter: function () {
                        return this.value;
                    }
                }
            },
            tooltip: data.tooltip,
            plotOptions: {
                series: {
                    fillOpacity: 1,
                    marker: {
                        enabled: data.marker
                    }
                },
                area: {
                    stacking: 'normal',
                    lineColor: '#666666',
                    lineWidth: 1,
                    marker: {
                        lineWidth: 1,
                        lineColor: '#666666'
                    }
                }
            },
            series: data.data
        });

    }
    exportData(filename) {
        this.buildChart.exportChartLocal({ filename: filename })
    }

    render() {
        const { id } = this.props.data;
        return <div id={id} className="stacked-area-chart-v" ref={el => { this.StackedAreaSVG = el; }} />
    }

}
