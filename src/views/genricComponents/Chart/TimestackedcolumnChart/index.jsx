import React, { Component } from 'react';
import Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting'
// import ExportingData from 'highcharts/modules/export-data'
import HichartsOfflineExporting from 'highcharts/modules/offline-exporting';
import HichartsBoost from 'highcharts/modules/boost';
Exporting(Highcharts)
// ExportingData(Highcharts)
HichartsOfflineExporting(Highcharts)
HichartsBoost(Highcharts)

export default class StackedColumnChart extends Component {
    enableSelection = false
    static defaultProps = {
        data: {
            id: "StackedColumnChart",
            title: "Stacked Column Chart",
            legend: {
                symbolWidth: 20,
                enabled: true
            },
            type: 'column',
            height: 305,
            plotOptions: {
                series: {
                    stacking: 'normal'
                },
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: 'white'
                    }
                }
            },
            tooltip: '{series.name}: <b>{point.percentage:.1f}%</b>',
            xAxis: {
                type: 'datetime'
            },
            stacklabeldata:[1,3,5],
            data: [{
                name: 'Planned',
                color: 'purple',
                data: [{
                    x: 1533081600000,
                    y: 1161
                },
                {
                    x: 1535760000000,
                    y: 1245
                }, {
                    x: 1537760000000,
                    y: 725
                }, {
                    x: 1539760000000,
                    y: 1145
                }, {
                    x: 1542760000000,
                    y: 945
                }, {
                    x: 1545760000000,
                    y: 1245
                }],
            }, {
                name: 'Actual',
                color: '#fb00a6',
                data: [{
                    x: 1533081600000,
                    y: 116
                },
                {
                    x: 1535760000000,
                    y: 124
                }, {
                    x: 1537760000000,
                    y: 735
                }, {
                    x: 1539760000000,
                    y: 145
                }, {
                    x: 1542760000000,
                    y: 9415
                }, {
                    x: 1545760000000,
                    y: 125
                }],
            },]
        }
    };

    buildChart = {}

    componentDidMount() {
        if (this.LineChartSVG !== undefined) {
            this.buildGraph(this.props.data);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.LineChartSVG !== undefined) {
            this.buildGraph(nextProps.data);
        }
    }

    buildGraph(data) {

        this.buildChart = Highcharts.chart(this.LineChartSVG, {
            chart: {
                type: data.type,
                height:data.height,
                zoomType: 'x'
            },
            boost: {
                useGPUTranslations: true
            },
            title: {
                text: ""
            },
            tooltip: data.tooltip,
            xAxis:data.xAxis,
            yAxis: {
                min: 0,
                title: {
                    text: ''
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'
                    },
                    formatter: function () {
                            return data.stacklabeldata[this.x];
                        }
                }
            },
            legend: data.legend,
            plotOptions: data.plotOptions,
            exporting:false,
            credits:false,
            series: data.data
        });
    }
    exportData(filename) {
        this.buildChart.exportChartLocal({ filename: filename })
    }

    render() {
        const { id } = this.props.data;
        return <div id={id} className="Line-chart-v" ref={el => { this.LineChartSVG = el; }} />
    }

}
