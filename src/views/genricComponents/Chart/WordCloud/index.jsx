import React, { Component } from 'react';
import Highcharts from 'highcharts';
import WordCloud from 'highcharts/modules/wordcloud';
// import Exporting from 'highcharts/modules/exporting';
import './style';

// Exporting(Highcharts)
WordCloud(Highcharts);

export default class WordCloudChart extends Component {
    enableWordClick = this.props.enableWordClick
    static defaultProps = {
        dataSet: {
            xTittle: 'string',
            yTittle: 'string',
            id: 'abc',
            data: [
                { name: 'Hey', weight: 1000 },
                { name: 'lol', weight: 200 },
                { name: 'first impression', weight: 800 },
                { name: 'very cool', weight: 1000000 },
                { name: 'duck', weight: 10 },
            ],
        },
        setWidth: 960,
        setHeight: 500,
    };

    buildChart = {}

    componentDidMount() {
        if (this.WordCloudSVG !== undefined) {
            this.buildGraph(this.props.data);
        }
    }
    componentWillReceiveProps(nextProps) {
        if (this.WordCloudSVG !== undefined) {
            this.buildGraph(nextProps.data);
        }
    }
    buildGraph(data) {
        let classThis = this
        Highcharts.chart(this.WordCloudSVG.id, {
            credits: {
                enabled: false
            },
            chart: {
                width: window.size,
                height: data.height,
            },
            title: {
                text: data.title
            },
            plotOptions: {
                series: {
                    cursor: 'pointer',
                    point: {
                        events: {
                            click: function () {
                                // if (enableWordClick) {
                                //     // uncomment after conversation popup
                                //     classThis.props.onWordClick(this.name)
                                // }
                            }
                        }
                    }
                }
            },
            series: [{
                rotation: {
                    from: 0,
                    to: 0,
                },
                color: "#9cbdc6",
                type: 'wordcloud',
                name: 'Count',
                data: data.data,
                dataLabels: {
                    enabled: true,
                    formatter: function () {
                        return this.value

                    }
                },
                // cursor: 'pointer',
                // events: {
                //     click: function (event) {
                //        
                //     }
                // }
            }],

            exporting: {
                enabled: false,
                chartOptions: {
                    plotOptions: {
                        series: {
                            dataLabels: {
                                enabled: true
                            }
                        }
                    }
                },
                fallbackToExportServer: false
            },
        });
    }
    render() {
        const { height, id } = this.props.data
        return <div id={id} ref={el => { this.WordCloudSVG = el; }} />
    }
}
