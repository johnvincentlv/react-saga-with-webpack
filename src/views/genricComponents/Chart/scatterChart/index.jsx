import React, { Component } from 'react';
import Highcharts from 'highcharts';
import Exporting from 'highcharts/modules/exporting'
// import ExportingData from 'highcharts/modules/export-data'
import HichartsOfflineExporting from 'highcharts/modules/offline-exporting';
Exporting(Highcharts)
// ExportingData(Highcharts)
HichartsOfflineExporting(Highcharts)

export default class ScatterChart extends Component {
    enableSelection = false
    static defaultProps = {
        data: {
            id: "scatter",
            title: "CSA Regional Deepdive",
            tooltip: '<div>{point.y}, {point.z} years<div>',
            yaxisTitle: "",
            margin: { top: 40, left: 80, bottom: 30, right: 40 },
            height: 225,
            enableSelection: false,
            type: 'bubble',
            categories: ['East', 'West', 'North', 'South'],
            series: [{
                name: 'Female',
                color: 'rgba(223, 83, 83, .5)',
                data: [[161.2, 51.6], [167.5, 59.0], [159.5, 49.2], [157.0, 63.0], [155.8, 53.6],
                    [170.0, 59.0], [159.1, 47.6], [166.0, 69.8], [176.2, 66.8], [160.2, 75.2],
                    [172.5, 55.2], [170.9, 54.2]]
        
            }, {
                name: 'Male',
                color: 'rgba(119, 152, 191, .5)',
                data: [[174.0, 65.6], [175.3, 71.8], [193.5, 80.7], [186.5, 72.6], [187.2, 78.8],
                    [181.5, 74.8], [184.0, 86.4], [184.5, 78.4], [175.0, 62.0], [184.0, 81.6],
                    [180.0, 76.6], [177.8, 83.6], [192.0, 90.0], [176.0, 74.6], [174.0, 71.0],
                    [184.0, 79.6], [192.7, 93.8], [171.5, 70.0], [173.0, 72.4], [176.0, 85.9],
                    [176.0, 78.8]]
            }]
        }
    };

    buildChart = {}

    componentDidMount() {
        if (this.ScatterChartSVG !== undefined) {
            this.buildGraph(this.props.data);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.ScatterChartSVG !== undefined) {
            this.buildGraph(nextProps.data);
        }
    }

    buildGraph(data) {
        this.buildChart = Highcharts.chart(this.ScatterChartSVG.id, {

            chart: {
                type: data.type,
                plotBorderWidth: 1,
                height:data.height,
            },
            legend: {
                enabled: false
            },
            title: {
                text: data.title
            },
            exporting: false,
            subtitle: {
                text: data.subtitle
            },
            plotOptions: {

            },
            xAxis: {
                gridLineWidth: 1,
                categories: data.categories,
                title: {
                    text: data.xaxisTitle
                },
                plotLines: [{
                    color: 'black',
                    width: 0,
                    value: 0,
                    zIndex: 3
                }]
            },
            credits: false,
            yAxis: {
                categories: data.ycategories,
                startOnTick: false,
                endOnTick: false,
                title: {
                    text: data.yaxisTitle
                },
                maxPadding: 0.2,
                plotLines: [{
                    color: 'black',
                    width: 0,
                    value: 0,
                    zIndex: 3
                }]
            },
            tooltip: data.tooltip,
            series: data.series
        });

    }
    exportData(filename) {
        this.buildChart.exportChartLocal({ filename: filename })
    }

    render() {
        const { id } = this.props.data;
        return <div id={id} className="scatter-chart-v" ref={el => { this.ScatterChartSVG = el; }} />
    }

}