import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Header from '../../layouts/Header';
import { Footer } from '../../layouts/Footer';

import MainPage from '../../pages';

const Root = (props) => {
    return (
        <div>
            <Header />
            <Switch>
                <Route path="/" component={MainPage} />
            </Switch>
            <Footer />
        </div>
    )
}

export default Root;