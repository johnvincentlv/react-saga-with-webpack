import React from 'react';
import './style.scss';

export const Footer = () => (<footer className="text-center">Powered By. Latent View Analytics</footer>);
