import React, { Component } from 'react';
import { connect } from 'react-redux';

import SideBar from '../SideBar/index';

import actionComponent1 from '../../../states/services/home/component1/action'

import "./style";

const mapStateToProps = (state, ownProps) => ({
    
});

const mapDispatchToProps = dispatch => ({
    actionComponent1: () => dispatch(actionComponent1()),
})

class Header extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        isOpen: false
      }
      this._menuToggle = this._menuToggle.bind(this);
      this._handleDocumentClick = this._handleDocumentClick.bind(this);
    }

    componentDidMount() {
      document.addEventListener('click', this._handleDocumentClick, false);
      this.props.actionComponent1()
    }
    componentWillUnmount() {
      document.removeEventListener('click', this._handleDocumentClick, false);
    }
    _handleDocumentClick(e) {
      if (!this.refs.root.contains(e.target) && this.state.isOpen === true) {
        this.setState({
        isOpen: false
      });
      };
    }
    _menuToggle(e) {
      e.stopPropagation();
      this.setState({
        isOpen: !this.state.isOpen
      });
    }
    render() {
      let menuStatus = this.state.isOpen ? 'isopen' : '';
  
      return (
        <div ref="root">
          <div className="menubar">
            <div className="hambclicker" onClick={ this._menuToggle }></div>
            <div id="hambmenu" className={ menuStatus }><span></span><span></span><span></span><span></span></div>
            <div className="title">
              <span>Demo APp</span>
            </div>
          </div>
          <SideBar menuStatus={ menuStatus }/>
        </div>
      )
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(Header);
