import React, { Component } from 'react';
import { connect } from 'react-redux';
import Area from './scene';

import "./style";

const mapStateToProps = (state, ownProps) => ({
    AreaChart: state.home.Area,
    isLoaded: state.home.isAreaLoaded,
});

const mapDispatchToProps = dispatch => ({

});
class HomeArea extends Component {
    componentWillReceiveProps = (nextProps) => {

    }
    changeInterval = (interval) => {
        this.setState({ active: interval })
    }
    chartOptions = () => {

    }

    render = () => {

        let chartProperty = {
            tooltip: "",
            margin: { top: 40, left: 80, bottom: 30, right: 40 },
            height: 380,
            lineColor: "#2CB0F7",
            lineWidth: 2,
            hoverLineWidth: 1,
            yAxis: {
                lineColor: "#CCD6EB",
                lineWidth: 1,
                gridLineWidth: 0,
                tickWidth: 2,
                min: 0,
                title: {
                    text: "values",
                    enabled: false
                }
            },
            marker: {
                radius: 3
            },
            gradientStartColor: '#EFFBFF',
            gradientEndColor: '#EFFBFF',
            enableSelection: false,
            enablePointClick: false,
            credits: { enabled: false },
            legend: { enabled: false },
            dataLabels: { enabled: true },
            exporting: { enabled: false },
        }
        
        let keys = _.keys(this.props.AreaChart)
        let AreaChart = []
        _.map(keys, (item, index) => {
            AreaChart.push({
                exportName: this.props.AreaChart[item].title,
                chartData: Object.assign({
                    id: this.props.AreaChart[item].id,
                    title: this.props.AreaChart[item].title,
                    data: this.props.AreaChart[item].data,
                }, chartProperty)
            })
        })
        return (
            <div className="col-12">
                <div className="row">
                    {this.props.isLoaded && _.map(AreaChart, (item,index) =><Area key = {index} data={item} />)}
                </div></div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeArea);
