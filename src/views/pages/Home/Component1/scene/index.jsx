import React, { Component } from 'react';
import { connect } from 'react-redux';
import Area from 'components/Chart/Area';

import "./style";

const mapStateToProps = (state, ownProps) => ({

});

const mapDispatchToProps = dispatch => ({
   
});
class HomeArea extends Component {
    state = {
        active:"YTD",
    };

    exportChart = (filename, ref) => {
        ref.exportData(filename);
    };
    componentWillReceiveProps = (nextProps) => {
        // this.setState([data]:nextprops)
    }
    
    changeInterval = (interval)=>{
        this.setState({active:interval})
    }
    chartOptions = () => {
        
    }

    render = () => {
        return (
            <div className="col-lg-6">
            <div className="chart">
                <div>                    
                    <Area data={this.props.data.chartData} ref= {(ref) => { this.chartName = ref; }} />
                </div>
                <div className="chart-tools">
                    <div>
                        {/* <i className="fas fa-download" onClick={(e) => this.exportChart(this.props.data.exportName, this.chartName)}></i>
                        <i className="fas fa-ellipsis-v" onClick={(e) =>this.chartOptions()}>
                        <div className="chartOptions">
                        </div>
                        </i> */}
                    </div>
                    <ul>
                        <li className={this.state.active==="YTD"?"chart-tool-active":""} onClick={(e)=>this.changeInterval("YTD")}>YTD</li>
                        <li className={this.state.active==="MTD"?"chart-tool-active":""} onClick={(e)=>this.changeInterval("MTD")}>MTD</li>
                        <li className={this.state.active==="WTD"?"chart-tool-active":""} onClick={(e)=>this.changeInterval("WTD")}>WTD</li>
                    </ul>
                </div>
            </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeArea);
