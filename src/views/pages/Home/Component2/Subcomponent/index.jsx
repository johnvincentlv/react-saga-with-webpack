import React, { Component } from 'react';
import { connect } from 'react-redux';
import Pie from './scene';

import "./style";

const mapStateToProps = (state, ownProps) => ({
    pie: state.home.pie,
    isLoaded: state.home.isPieLoaded,
});

const mapDispatchToProps = dispatch => ({

});
class HomePie extends Component {

    changeInterval = (interval) => {
        this.setState({ active: interval })
    }


    render = () => {
        let chartProperty = {
            tooltip: '{series.name}: <b>{point.percentage:.1f}%</b>',
            margin: { top: 40, left: 20, bottom: 30, right: 40 },
            height: 250,
            color: '#12b0f2',
            innerSize: '50%',
        };
        let keys = _.keys(this.props.pie)
        let pie = []
        _.map(keys, (item, index) => {
            pie.push({
                exportName: this.props.pie[item].title,
                chartData: Object.assign({
                    id: this.props.pie[item].id,
                    title: this.props.pie[item].title,
                    data: this.props.pie[item].data,
                }, chartProperty)
            })
        });
        return (
            <div className="col-12">
                <div className="row">
                    {this.props.isLoaded && _.map(pie, (item, index) => <Pie key={index} data={item} />)}
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePie);
