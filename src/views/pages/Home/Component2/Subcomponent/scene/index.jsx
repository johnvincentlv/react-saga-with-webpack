import React, { Component } from 'react';
import { connect } from 'react-redux';
import Pie from 'components/Chart/PieChart';

import "./style";

const mapStateToProps = (state, ownProps) => ({

});

const mapDispatchToProps = dispatch => ({

});
class HomePie extends Component {
    state = {
        active: "YTD",
    };

    exportChart = (filename, ref) => {
        ref.exportData(filename);
    };
    componentWillReceiveProps = (nextProps) => {
        // this.setState([data]:nextprops)
    }
    changeInterval = (interval) => {
        this.setState({ active: interval })
    }
    chartOptions = () => {

    }

    render = () => {
        return (
            <div className="col-lg-4">
                <div className="chart">
                    <div>
                        <Pie data={this.props.data.chartData} ref={(ref) => { this.chartName = ref; }} />
                    </div>
                    <div className="chart-tools">
                        <div>
                            {/* <i className="fas fa-download" onClick={(e) => this.exportChart(this.props.data.exportName, this.chartName)}></i>
                            <i className="fas fa-ellipsis-v"></i> */}
                        </div>
                        <ul>
                            <li className={this.state.active === "YTD" ? "chart-tool-active" : ""} onClick={(e) => this.changeInterval("YTD")}>YTD</li>
                            <li className={this.state.active === "MTD" ? "chart-tool-active" : ""} onClick={(e) => this.changeInterval("MTD")}>MTD</li>
                            <li className={this.state.active === "WTD" ? "chart-tool-active" : ""} onClick={(e) => this.changeInterval("WTD")}>WTD</li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePie);
