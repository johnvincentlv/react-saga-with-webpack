import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import "./style";
import  actionComponent1 from 'states/services/home/component1/action';

const mapStateToProps = (state, ownProps) => {
    return{
        vale:state.home.component1.data    
    }
};

const mapDispatchToProps = dispatch => ({
    actionComponent1: () => dispatch(actionComponent1()),
});


class Home extends Component {

    componentWillMount = () => {
        this.props.actionComponent1();
    }


    render = () => {
        return (
            <div>
                <div className="row">
                    <div className="col-12">
                        <div className="home-view">
                            <h5>Dashbaord with SAGA with {this.props.vale}</h5>
                        </div>

                    </div>

                </div>
                {/* <HomeMap/> */}
                
            </div>
        )
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(Home);
