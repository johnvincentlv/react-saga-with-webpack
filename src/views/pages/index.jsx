import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

import Home from './Home';


class Dashboard extends Component {

    render = () => {
        return (
            <main className="main-container">
                <div className="col-md-10 ml-sm-auto col-lg-10 ">
                    <Switch>
                        <Route exact path='/' component={Home} />
                    </Switch >
                </div>
            
            </main>
        )
    }
}
export default Dashboard

